﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrownScript : MonoBehaviour {
    SpriteAnimator spriteAnimator;

	// Use this for initialization
	void Start () {
        spriteAnimator = GetComponent<SpriteAnimator>();
        spriteAnimator.PlayAnimation(0);
    }

}
