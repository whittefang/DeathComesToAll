﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class  SpriteAnimator : MonoBehaviour {
    [SerializeField]
    public List<SpriteAnimation> animations;
    public SpriteRenderer spriteRender;
    int currentAnimation = -1;
    public void Awake(){
        

    }

    public int GetCurrentAnimation()
    {
        return currentAnimation;
    }

    public void PlayAnimation(int key)
    {
        if (animations[key].animationType == AnimationType.OneShot)
        {
            StopAllCoroutines();
            StartCoroutine(AnimateOneShot(animations[key]));
        }
        else if (animations[key].animationType == AnimationType.Loop && currentAnimation != key)
        {
            StopAllCoroutines();
            StartCoroutine(AnimateLoop(animations[key]));

        }
        currentAnimation = key;
    }

    void OnDisable() {
        StopAllCoroutines();
        currentAnimation = -1;
    }

    IEnumerator AnimateOneShot(SpriteAnimation animation) {
        for (int i = 0; i < animation.frames.Length; i++)
        {
            spriteRender.sprite = animation.frames[i];
            for (int ii = 0; ii < animation.timeBetweenFrames; ii++)
            {
                yield return null;
            }
        }
        currentAnimation = -1;
    }
    IEnumerator AnimateLoop(SpriteAnimation animation)
    {
        int currentFrame = 0;
        while(true)
        {
            spriteRender.sprite = animation.frames[currentFrame];
            currentFrame++;
            if (currentFrame >= animation.frames.Length) {
                currentFrame = 0;
            }
            for (int ii = 0; ii < animation.timeBetweenFrames; ii++)
            {
                yield return null;
            }
        }
    }
}
