﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelectUI : MonoBehaviour {
    public GameObject[] weaponButtonGraphics;
    public GameObject[] movementButtonGraphics;
    public GameObject[] colorButtonGraphics;

    public GameObject[] weaponBackgroundGraphics;
    public GameObject[] movementBackgroundGraphics;
    public GameObject joinImage;
    public int playerNumber;
    [SerializeField] MenuState menuState, prevMenuState;
    InputScript inputScript;
    MasterGameObject masterGameObject;
    public int currentWeapon, currentMovement, currentColor;
    // Use this for initialization
    void Start () {
        inputScript = GetComponent<InputScript>();
        inputScript.assignAButton(Accept, null);
        inputScript.assignBButton(Back, null);
        inputScript.assignLeftStickLeftRightPress(ChangeSelectionLeft, ChangeSelectionRight);
        inputScript.assignLeftStickUpDownPress(ChangeSelectionUp, ChangeSelectionDown);
        menuState = MenuState.Join;
        masterGameObject = GameObject.Find("MasterGameObject").GetComponent<MasterGameObject>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void Accept() {
        if (menuState == MenuState.Join) {
            menuState = MenuState.SelectWeapon;
            joinImage.SetActive(false);
        }
        else if (menuState == MenuState.SelectWeapon || menuState == MenuState.SelectMovement || menuState == MenuState.SelectColor)
        {
            prevMenuState = menuState;
            menuState = MenuState.Ready;
            masterGameObject.SetPlayerEquipment(inputScript.playerNumber, currentWeapon, currentMovement, currentColor);
            masterGameObject.AddPlayerToGame(inputScript.playerNumber);
        }
        
    }
    void Back() {
        if (menuState == MenuState.Ready)
        {
            menuState = prevMenuState;
            masterGameObject.RemovePlayerFromGame(inputScript.playerNumber);
        }
        else if (menuState == MenuState.SelectWeapon || menuState == MenuState.SelectMovement || menuState == MenuState.SelectColor)
        {
            menuState = MenuState.Join;
            joinImage.SetActive(true);
        }
    }
    int MoveIndex(int direction, GameObject[] type, int currentSpot) {
        type[currentSpot].SetActive(false);
        currentSpot += direction;
        if (currentSpot >= type.Length) {
            currentSpot = 0;
        } else if (currentSpot < 0) {
            currentSpot = type.Length-1;
        }
        type[currentSpot].SetActive(true);
        return currentSpot;
    }
    void ChangeSelectionLeft()
    {
        if (menuState == MenuState.SelectWeapon)
        {
            currentWeapon = MoveIndex(-1, weaponButtonGraphics, currentWeapon);
        }
        else if (menuState == MenuState.SelectMovement)
        {
            currentMovement = MoveIndex(-1, movementButtonGraphics, currentMovement);
        }
        else if (menuState == MenuState.SelectColor)
        {
            currentColor = MoveIndex(-1, colorButtonGraphics, currentColor);
        }
    }
    void ChangeSelectionRight()
    {
        if (menuState == MenuState.SelectWeapon)
        {
            currentWeapon = MoveIndex(1, weaponButtonGraphics, currentWeapon);
        }
        else if (menuState == MenuState.SelectMovement)
        {
            currentMovement = MoveIndex(1, movementButtonGraphics, currentMovement);
        }
        else if (menuState == MenuState.SelectColor)
        {
            currentColor = MoveIndex(1, colorButtonGraphics, currentColor);
        }
    }
    void ChangeSelectionUp()
    {
        if (menuState == MenuState.SelectWeapon)
        {
            menuState = MenuState.SelectColor;

        }
        else if (menuState == MenuState.SelectMovement)
        {
            menuState = MenuState.SelectWeapon;

        }
        else if (menuState == MenuState.SelectColor)
        {
            menuState = MenuState.SelectMovement;
        }
    }
    void ChangeSelectionDown()
    {
        if (menuState == MenuState.SelectWeapon)
        {
            menuState = MenuState.SelectMovement;
        }
        else if (menuState == MenuState.SelectMovement)
        {
            menuState = MenuState.SelectColor;
        }
        else if (menuState == MenuState.SelectColor)
        {
            menuState = MenuState.SelectWeapon;

        }
    }
}
enum MenuState{
    Join,
    SelectWeapon,
    SelectMovement,
    SelectColor,
    Ready
}
