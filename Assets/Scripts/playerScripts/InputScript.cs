﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure; // Required in C#

public class InputScript : MonoBehaviour {
	public delegate void buttonDelegate();
	public delegate void ThumbstickDelegate(float x, float y);
	buttonDelegate  startButtonPress, backButtonPress, aButtonPress, bButtonPress, xButtonPress, yButtonPress, aButtonRelease, bButtonRelease, xButtonRelease, yButtonRelease,
	rbButtonPress, rbButtonRelease, rTriggerPress, rTriggerRelease,
        leftStickRightPress, leftStickLeftPress, leftStickUpPress, leftStickDownPress;
	ThumbstickDelegate leftStick;

	PlayerIndex playerIndex;
	GamePadState state;
	GamePadState prevState;
	public bool inputEnabled = true;
	public int playerNumber = 0;
	// Use this for initialization
	void Start () {
        
		
		playerIndex = (PlayerIndex)playerNumber;

	}

	// Update is called once per frame
	void FixedUpdate () {
		if (inputEnabled) {
			prevState = state;
			state = GamePad.GetState (playerIndex, GamePadDeadZone.None);

            // send input for movement
            if (leftStick != null)
            {
                leftStick(state.ThumbSticks.Left.X, state.ThumbSticks.Left.Y);
            }
			
			// Detect if start button was pressed this frame
			if (prevState.Buttons.Start == ButtonState.Released && state.Buttons.Start == ButtonState.Pressed && startButtonPress != null) {
				startButtonPress ();
			}
			// Detect if back button was pressed this frame
			if (prevState.Buttons.Back == ButtonState.Released && state.Buttons.Back == ButtonState.Pressed && backButtonPress != null) {
				backButtonPress ();
			}
			// Detect if a button was pressed this frame
			if (prevState.Buttons.A == ButtonState.Released && state.Buttons.A == ButtonState.Pressed && aButtonPress != null) {
				aButtonPress ();
			}
			// Detect if a button was released this frame
			if (prevState.Buttons.A == ButtonState.Pressed && state.Buttons.A == ButtonState.Released && aButtonRelease != null) {
				aButtonRelease ();
			}

			// Detect if a button was pressed this frame
			if (prevState.Buttons.X == ButtonState.Released && state.Buttons.X == ButtonState.Pressed && xButtonPress != null) {
			
				xButtonPress ();
			}
			// Detect if a button was released this frame
			if (prevState.Buttons.X == ButtonState.Pressed && state.Buttons.X == ButtonState.Released && xButtonRelease != null) {
			
				xButtonRelease ();
			}

			// Detect if a button was pressed this frame
			if (prevState.Buttons.Y == ButtonState.Released && state.Buttons.Y == ButtonState.Pressed && yButtonPress != null) {
				yButtonPress ();
			}
			// Detect if a button was released this frame
			if (prevState.Buttons.Y == ButtonState.Pressed && state.Buttons.Y == ButtonState.Released && yButtonRelease != null) {
				yButtonRelease ();
			}

			// x button press
			if (state.Buttons.B == ButtonState.Pressed && prevState.Buttons.B == ButtonState.Released && bButtonPress != null) {
				bButtonPress ();
			}
			// Detect if a button was released this frame
			if (prevState.Buttons.B == ButtonState.Pressed && state.Buttons.B == ButtonState.Released && bButtonRelease != null) {
				bButtonRelease ();
			}
			// x button press
			if (state.Buttons.RightShoulder == ButtonState.Pressed && prevState.Buttons.RightShoulder == ButtonState.Released && rbButtonPress != null) {
				rbButtonPress ();
			}
			// Detect if a button was released this frame
			if (prevState.Buttons.RightShoulder == ButtonState.Pressed && state.Buttons.RightShoulder == ButtonState.Released && rbButtonRelease != null) {
				rbButtonRelease ();
			}
			// Detect if rt button was released this frame
			if (prevState.Triggers.Right > .5f && state.Triggers.Right < .5f && rTriggerRelease != null) {
				rTriggerRelease ();
			}
			// Detect if rt button was pressed this frame
			if (prevState.Triggers.Right < .5f && state.Triggers.Right > .5f && rTriggerPress != null) {
				rTriggerPress ();
			}
            if (state.ThumbSticks.Left.X > .5f && prevState.ThumbSticks.Left.X < .5f && leftStickRightPress != null) {
                leftStickRightPress();
            }
            if (state.ThumbSticks.Left.X < -.5f && prevState.ThumbSticks.Left.X > -.5f && leftStickLeftPress != null)
            {
                leftStickLeftPress();
            }
            if (state.ThumbSticks.Left.Y > .5f && prevState.ThumbSticks.Left.Y < .5f && leftStickUpPress != null)
            {
                leftStickUpPress();
            }
            if (state.ThumbSticks.Left.Y < -.5f && prevState.ThumbSticks.Left.Y > -.5f && leftStickDownPress != null)
            {
                leftStickDownPress();
            }

        }
	}
    // takes in function delegate and assigns them to appropriate buttons
    public void assignLeftStickLeftRightPress(buttonDelegate leftPress, buttonDelegate rightPress)
    {
        leftStickRightPress = rightPress;
        leftStickLeftPress = leftPress;
    }
    // takes in function delegate and assigns them to appropriate buttons
    public void assignLeftStickUpDownPress(buttonDelegate upPress, buttonDelegate downPress)
    {
        leftStickUpPress = upPress;
        leftStickDownPress = downPress;
    }
    // takes in function delegate and assigns them to appropriate buttons
    public void assignStartButton(buttonDelegate aPress, buttonDelegate aRelease){
		startButtonPress = aPress;
	}
	// takes in function delegate and assigns them to appropriate buttons
	public void assignbackButton(buttonDelegate aPress, buttonDelegate aRelease){
		backButtonPress = aPress;
	}
	// takes in function delegate and assigns them to appropriate buttons
	public void assignAButton(buttonDelegate aPress, buttonDelegate aRelease){
		aButtonPress = aPress;
		aButtonRelease = aRelease;
	}
	// takes in function delegate and assigns them to appropriate buttons
	public void assignBButton(buttonDelegate bPress, buttonDelegate bRelease){
		bButtonPress = bPress;
		bButtonRelease = bRelease;
	}
	// takes in function delegate and assigns them to appropriate buttons
	public void assignXButton(buttonDelegate xPress, buttonDelegate xRelease){
		xButtonPress = xPress;
		xButtonRelease = xRelease;
	}
	// takes in function delegate and assigns them to appropriate buttons
	public void assignYButton(buttonDelegate yPress, buttonDelegate yRelease){
		yButtonPress = yPress;
		yButtonRelease = yRelease;
	}
	// takes in function delegate and assigns them to appropriate buttons
	public void assignRBButton(buttonDelegate rbPress, buttonDelegate rbRelease){
		rbButtonPress = rbPress;
		rbButtonRelease = rbRelease;
	}
	// takes in function delegate and assigns them to appropriate buttons
	public void assignRT(buttonDelegate rtPress, buttonDelegate rtRelease){
		rTriggerPress = rtPress;
		rTriggerRelease = rtRelease;
	}
	public void SetThumbstick(ThumbstickDelegate newLeftStick){
		leftStick = newLeftStick;
	}
	public void SetPlayerNumber (int newNum){
		playerNumber = newNum;
		playerIndex = (PlayerIndex)playerNumber;
	}
	public float GetX(bool raw = true){
		if (raw) {
			return state.ThumbSticks.Left.X;
		} else if (Mathf.Abs(state.ThumbSticks.Left.X) > .2f){
			return state.ThumbSticks.Left.X;
		}else {
			return 0;
		}

	}
	public float GetY(bool raw = true){
		if (raw) {
			return state.ThumbSticks.Left.Y;
		} else if (Mathf.Abs(state.ThumbSticks.Left.Y) > .2f){

			return state.ThumbSticks.Left.Y;
		}else {
			return 0;
		}
		
	}
	public bool CheckRTRelease(){
		if (state.Triggers.Right > .5f) {
			return true;
		} else {
			return false;
		}
	}
}
