﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Weapon {

    void PressAttackPrimary();
    void ReleaseAttackPrimary();
    void PressAttackSecondary();
    void ReleaseAttackSecondary();
    void CancelAttacks();
}
