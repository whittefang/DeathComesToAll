﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathHandler : MonoBehaviour {
    public GameObject GhostBody;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Kill() {
        GhostBody.SetActive(true);
        gameObject.SetActive(false);

    }
}
