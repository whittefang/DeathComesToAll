﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitResolver : MonoBehaviour {
    InputScript input;
    Health health;
    Score score;
    // Use this for initialization
    void Start () {
        score = GameObject.Find("ScoreCrown").GetComponent<Score>();
        input = GetComponent<InputScript>();
        health = GetComponent<Health>();	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Hitbox")
        {
            Hitbox otherHitbox = other.GetComponent<Hitbox>();
            if (!otherHitbox.ComparePlayerID(input.playerNumber)) {
                score.addScore(otherHitbox.getPlayerNumber());
                health.DealDamage(100);

            }
        }
    }
}
