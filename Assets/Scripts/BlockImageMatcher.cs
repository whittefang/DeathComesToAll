﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockImageMatcher : MonoBehaviour {
    public Sprite[] blockImages;

    SpriteRenderer[,] blocks;

    // Use this for initialization
    void Start () {
        blocks = new SpriteRenderer[19,11];
        LoadInBlocks();

    }
    void LoadInBlocks()
    {
        foreach (SpriteRenderer child in gameObject.GetComponentsInChildren<SpriteRenderer>())
        {
            blocks[(int)child.transform.localPosition.x+9, (int)child.transform.localPosition.y+5] = child;
        }
        for (int i = 0; i < blocks.GetLength(0); i++)
        {
            for (int ii = 0; ii < blocks.GetLength(1); ii++)
            {
                if (blocks[i,ii] != null) {
                    SetPictureForBlock(i, ii);
                }
            }
        }
    }

    void SetPictureForBlock(int x, int y) {
        int imageKey = 0;
        // touching right
        if (blocks.GetLength(0) <= x+1 ||  blocks[x + 1, y] != null) {
            imageKey += 4;
        }
        // touching left
        if ((x - 1) < 0 ||  blocks[x - 1, y] != null)
        {
            imageKey += 1;
        }
        // touching top
        if (blocks.GetLength(1) <= y + 1 || blocks[x , y+1] != null)
        {
            imageKey += 2;
        }
        // touching bottom
        if ((y - 1) < 0 || blocks[x, y-1] != null)
        {
            imageKey += 8;
        }
        blocks[x, y].sprite = blockImages[imageKey];

        

    }
	// Update is called once per frame
	void Update () {
		
	}
}
