﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectUI : MonoBehaviour
{
    public GameObject selectionIcon;
    public LevelSelectIcon[,] levels;
    public SpriteRenderer previewImage;
    public Vector2 currentSelection;
    InputScript input;

	// Use this for initialization
	void Start () {
        levels = new LevelSelectIcon[5, 1];
        input = GetComponent<InputScript>();
        input.assignLeftStickLeftRightPress(MoveLeft, MoveRight);
        input.assignLeftStickUpDownPress(MoveUp, MoveDown);
        input.assignAButton(ConfirmSelection, null);
        PopulateLevels();
    }

    void PopulateLevels() {
        foreach (LevelSelectIcon current in gameObject.GetComponentsInChildren<LevelSelectIcon>()) {
            // very important, the locations are dependant on locations
            // y will have to be adjusted and x may have to be adjusted based on any change in spacing
            levels[ (int)(current.transform.position.x / 3) + 2, (int)current.transform.position.y] = current;
        }
    }


    void ConfirmSelection() {
        // add logic to load in appropriate level
    }

    void MoveLeft() {
        CheckForValidMove(currentSelection + new Vector2(-1, 0));
    }
    void MoveRight()
    {
        CheckForValidMove(currentSelection + new Vector2(1, 0));
    }
    void MoveUp()
    {
        CheckForValidMove(currentSelection + new Vector2(0, 1));
    }
    void MoveDown()
    {
        CheckForValidMove(currentSelection + new Vector2(0, -1));
    }

    public void CheckForValidMove(Vector2 proposedPosition) {
        Debug.Log(proposedPosition);
        if (proposedPosition.x >= levels.GetLength(0))
        {
            proposedPosition.x = 0;
        }
        else if (proposedPosition.x < 0)
        {
            proposedPosition.x = levels.GetLength(0) - 1;
        }

        if (proposedPosition.y >= levels.GetLength(1))
        {
            proposedPosition.y = 0;
        }
        else if (proposedPosition.y < 0)
        {
            proposedPosition.y = levels.GetLength(1) - 1;
        }

        MoveCursor(proposedPosition);
    }
    void MoveCursor(Vector2 proposedPosition) {
        Debug.Log(proposedPosition);
        currentSelection = proposedPosition;
        selectionIcon.transform.position = levels[(int)currentSelection.x, (int)currentSelection.y].GetIconLocation();
        previewImage.sprite = levels[(int)currentSelection.x, (int)currentSelection.y].previewImage;
    }
}
