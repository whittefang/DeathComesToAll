﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPlayer : MonoBehaviour {
    InputScript input;
    MovementEquipment movementEquipment;
    // Use this for initialization
    void Start()
    {
        input = GetComponent<InputScript>();
        input.assignRT(StartPrimaryAttackPress, StartPrimaryAttackRelease);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void StartPrimaryAttackPress()
    {
        movementEquipment.PressMovement();
    }

    void StartPrimaryAttackRelease()
    {
        movementEquipment.ReleaseMovement();
    }

   
    public void CancelMovement()
    {
        movementEquipment.CancelMovement();
    }
    public void SetMovement(MovementEquipment newMovementEquipment)
    {
        movementEquipment = newMovementEquipment;
    }
}
