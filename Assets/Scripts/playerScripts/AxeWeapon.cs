﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeWeapon : MonoBehaviour, Weapon {
    public GameObject axeHitbox;
    public GameObject axeThrowHitbox;
    HandAnimationController handAnimator;
    Rigidbody2D throwHitboxRB;
    AxeState axeState = AxeState.Neutral;
    PlayerState playerState;
    bool canPickUpAxe = false;

    void Awake() {
        handAnimator = transform.parent.GetChild(1).GetComponent<HandAnimationController>();
        handAnimator.SetAnimator(gameObject.GetComponent<SpriteAnimator>());
        gameObject.GetComponent<SpriteAnimator>().spriteRender = handAnimator.GetComponent<SpriteRenderer>();
        playerState = GetComponentInParent<PlayerState>();
        transform.position = Vector2.zero;
        throwHitboxRB = GetComponentInChildren<Rigidbody2D>(true);
    }

    public void CancelAttacks()
    {
        if (axeState == AxeState.Attacking)
        {
            axeState = AxeState.Neutral;
        }
    }

    public void PressAttackPrimary()
    {
        if (axeState == AxeState.Neutral)
        {
            StopAllCoroutines();
            StartCoroutine(SwingAxe());
        }
    }

    public void PressAttackSecondary()
    {
        if (axeState == AxeState.Neutral)
        {
            StartCoroutine(ThrowAxe());
        }
       
    }

    public void ReleaseAttackPrimary()
    {
        
    }

    public void ReleaseAttackSecondary()
    {
       
    }

    void OnTriggerEnter2D(Collider2D other) {

        if (other.tag == "Hitbox" && canPickUpAxe && other.GetComponent<ThrownAxe>() != null ) {
            
            other.gameObject.SetActive(false);
            axeState = AxeState.Neutral;
            handAnimator.ChangeGenericAnims(0, 1, 2);
        }
    }
    IEnumerator ThrowAxe() {
        handAnimator.PlayAnimation(6);
        handAnimator.ChangeGenericAnims(7,8,9);
        axeState = AxeState.NoSword;
        for (int i = 0; i < 10; i++)
        {
            yield return null;
        }
        StartCoroutine(AxePickupBuffer());
        axeThrowHitbox.transform.position = transform.position;
        axeThrowHitbox.transform.position += new Vector3(0, .5f);
        axeThrowHitbox.SetActive(true);
        if (playerState.IsFacingRight())
        {
            throwHitboxRB.velocity = new Vector2(20, 10);
        }
        else {
            throwHitboxRB.velocity = new Vector2(-20, 10);
        }
    }

    IEnumerator SwingAxe()
    {
        playerState.SetAttacking(true);
        axeState = AxeState.Attacking;


        int duration = 30;
        float flipRotation = 180;

        if (!playerState.IsFacingRight())
        {
            flipRotation = 0;
        }

        handAnimator.PlayAnimation(5);

        transform.eulerAngles = new Vector3(0, flipRotation, 0);
       

        for (int i = 0; i < duration; i++)
        {
            if (i == 4) {
                axeHitbox.SetActive(true);
                axeHitbox.transform.localPosition= new Vector2(.5f, -1.2f);
                axeHitbox.transform.localEulerAngles = new Vector3(0,0,-45);
            }
            if (i == 8)
            {
                axeHitbox.transform.localPosition = new Vector2(-.2f, -1.5f);
                axeHitbox.transform.localEulerAngles = new Vector3(0, 0,- 60);
            }
            if (i == 12)
            {
                axeHitbox.transform.localPosition = new Vector2(-.45f, -1f);
                axeHitbox.transform.localEulerAngles = new Vector3(0, 0, -90);
            }
            if (i == 16)
            {
                axeHitbox.transform.localPosition = new Vector2(-1f, -.9f);
                axeHitbox.transform.localEulerAngles = new Vector3(0, 0, 50);
            }
            if (i == 20)
            {
                axeHitbox.transform.localPosition = new Vector2(-1f, -.04f);
                axeHitbox.transform.localEulerAngles = new Vector3(0, 0, 0);
            }
            if (i == 24)
            {
                axeHitbox.transform.localPosition = new Vector2(-.92f, .6f);
                axeHitbox.transform.localEulerAngles = new Vector3(0, 0, -45);
            }
            yield return null;

        }

        axeHitbox.SetActive(false);
        for (int i = 0; i < 10; i++)
        {
            yield return null;
        }
        axeState = AxeState.Neutral;
        playerState.SetAttacking(false);
    }
    IEnumerator AxePickupBuffer() {
        canPickUpAxe = false;
        yield return new WaitForSeconds(.05f);
        canPickUpAxe = true;
    }
    enum AxeState {
        Neutral,       
        Attacking,
        NoSword
    }
}
