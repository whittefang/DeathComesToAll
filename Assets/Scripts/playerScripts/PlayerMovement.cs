﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    PlayerState playerState;
    Rigidbody2D rb;
    InputScript input;
    public SpriteAnimator spriteAnimator;
    public HandAnimationController handAnimator;

    const float GROUND_SPEED = 10;
    const float AIR_SPEED = 2;
    const float AIR_SPEED_MAX = 12;
    const float JUMP_FORCE = 900;
    // Use this for initialization
    void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        playerState = GetComponent<PlayerState>();
        input = GetComponent<InputScript>();
        
        input.assignAButton(Jump, null);
	}
	
	// Update is called once per frame
	void Update () {
        MoveHorizontal(input.GetX(false));
        MoveVertical(input.GetY(false));
    }

    //--------------------//
    // Movement Block Horizontal
    //--------------------//
    void MoveHorizontal(float xThumbstickValue) {
        if (!playerState.IsLeftRightEnabled()) {
            return;
        }

        if (playerState.GetState() == CharacterState.air) {
            MoveHorizontalAir(xThumbstickValue);
        }
        else if (playerState.GetState() == CharacterState.ground){
            MoveHorizontalGround(xThumbstickValue);
        }
        else if (playerState.GetState() == CharacterState.wall){
            MoveHorizontalWall(xThumbstickValue);
        }
    }

    void MoveHorizontalAir(float xThumbstickValue) {
        if (Mathf.Abs(xThumbstickValue) > 0)
        {
            if ((rb.velocity.x < AIR_SPEED_MAX && xThumbstickValue > 0 )|| (rb.velocity.x > -AIR_SPEED_MAX && xThumbstickValue < 0))
            {
                rb.velocity += new Vector2(AIR_SPEED * xThumbstickValue, 0);
            }
        }
        if (rb.velocity.x > 1)
        {
            playerState.setFacingRight(true);
        }
        else if (rb.velocity.x < -1)
        {
            playerState.setFacingRight(false);
        }
    }

    void MoveHorizontalGround(float xThumbstickValue)
    {
        rb.velocity = new Vector2(xThumbstickValue * GROUND_SPEED, rb.velocity.y);
        if (rb.velocity.x > 1)
        {
            playerState.setFacingRight(true);
            spriteAnimator.PlayAnimation(1);
            handAnimator.PlayRunning();
        }
        else if (rb.velocity.x < -1)
        {
            playerState.setFacingRight(false);
            spriteAnimator.PlayAnimation(1);
            handAnimator.PlayRunning();
        }
        else {
            spriteAnimator.PlayAnimation(0);
            handAnimator.PlayNeutral();
        }
    }

    void MoveHorizontalWall(float xThumbstickValue)
    {

    }
    //--------------------//
    // Movement Block Vertical
    //--------------------//
    void MoveVertical(float yThumbstickValue)
    {
        
        if (playerState.GetState() == CharacterState.air)
        {
           
        }
        else if (playerState.GetState() == CharacterState.ground)
        {
            
        }
        else if (playerState.GetState() == CharacterState.wall)
        {
            rb.velocity = new Vector2(0, -3f);
        }
    }
    //--------------------//
    // Jump Block
    //--------------------//

    void Jump() {
        if (playerState.GetState() == CharacterState.air)
        {
            JumpAir();
            spriteAnimator.PlayAnimation(2);
            handAnimator.PlayJump();
        }
        else if (playerState.GetState() == CharacterState.ground)
        {
            JumpGround();
            spriteAnimator.PlayAnimation(2);
            handAnimator.PlayJump();
        }
        else if (playerState.GetState() == CharacterState.wall)
        {
            JumpWall();
            spriteAnimator.PlayAnimation(2);
            handAnimator.PlayJump();
        }
    }

    void JumpAir()
    {
        if (playerState.IsDoubleJumpRemaining())
        {
            playerState.UseUpDoubleJump();
            if (Mathf.Abs( rb.velocity.x )< AIR_SPEED_MAX)
            {
                if (input.GetX(false) > 0)
                {
                    rb.velocity = new Vector2(AIR_SPEED_MAX, 0);
                }
                else if (input.GetX(false) < 0)
                {
                    rb.velocity = new Vector2(-AIR_SPEED_MAX, 0);
                }
                else {
                    rb.velocity = new Vector2(0, 0);
                }
            }
            else {
                rb.velocity = new Vector2(rb.velocity.x, 0);
            }
           
            rb.AddForce(new Vector2(0, JUMP_FORCE));
        }
    }

    void JumpGround() {

        rb.AddForce(new Vector2(0, JUMP_FORCE));
        playerState.UseGroundJump();
    }
    void JumpWall()
    {
        if (playerState.IsOnWallLeft()){
            rb.velocity = new Vector2(AIR_SPEED_MAX, 0);
            rb.AddForce(new Vector2(0, JUMP_FORCE));
        }
        else
        {
            rb.velocity = new Vector2(-AIR_SPEED_MAX, 0);
            rb.AddForce(new Vector2(0, JUMP_FORCE));
        }
        playerState.UseWallJump();
    }



    

}
