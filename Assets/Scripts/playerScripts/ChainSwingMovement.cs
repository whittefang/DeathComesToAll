﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainSwingMovement : MonoBehaviour, MovementEquipment {
    public GameObject grappleAnchor;
    public LineRenderer lineRenderer;

    PlayerState playerState;
    Rigidbody2D rb;
    Vector2 grappleDirection;
    Vector2 swingPoint;
    Vector3 prevPosition;
    float swingRadius;
    bool swingEnabled = true;
    int groundMask;

    // Use this for initialization
    void Awake () {
       groundMask = 1 << 8;
        playerState = GetComponentInParent<PlayerState>();
        rb = GetComponentInParent<Rigidbody2D>();

    }
   
	
	// Update is called once per frame
	void FixedUpdate () {
        UpdateChain();
        prevPosition = transform.parent.position;
    }
   

    public void PressMovement()
    {
        ChainSwingPress();
    }

    public void ReleaseMovement()
    {
        BreakLine();
    }

    // ----------------- // section of controls for swinging chain \\ ----------------- \\
    void UpdateChain(bool firstFrame = false)
    {
        // change up velocity based on swinging state
        if (playerState.GetState() == CharacterState.special)
        {
            swingPoint = grappleAnchor.transform.position;
            CheckLineBreaks();


            ReelInChain();
            LineGraphicsUpdate();
            if (Vector2.Distance(transform.parent.position, swingPoint) > swingRadius)
            {

                transform.parent.position = Vector2.MoveTowards(transform.parent.position, swingPoint, Vector2.Distance(transform.parent.position, swingPoint) - swingRadius);
                rb.velocity = ((transform.parent.position - prevPosition) / Time.deltaTime);
                if (firstFrame)
                {
                    rb.velocity = rb.velocity * 25;
                    /*if (facingRight && rb.velocity.x < 10) {
						rb.velocity = new Vector3(10, rb.velocity.y, 0);
					} else if (rb.velocity.x > -10){
						rb.velocity = new Vector3(-10, rb.velocity.y, 0);
					}*/
                }
            }
        }
    }
    // seperate button for reeling in the line
    public void ReelInChain()
    {
        if (playerState.GetState() == CharacterState.special)
        {
            // reel in line
            if (swingRadius > 2)// && !BlockReelIn)
            {
                swingRadius -= .05f;
            }
        }
    }
    // handles inital button press for swinging 
    public void ChainSwingPress()
    {
        if (playerState.GetState() == CharacterState.air && swingEnabled)
        {
           if (playerState.IsFacingRight())
            {
                grappleDirection = new Vector2(1, 1);
            }
            else {
                grappleDirection = new Vector2(-1, 1);
            }

            RaycastHit2D swingHit = Physics2D.Raycast(transform.parent.position, grappleDirection, Mathf.Infinity, groundMask);
            swingPoint = swingHit.point;
            swingRadius = swingHit.distance;
            grappleAnchor.transform.position = swingPoint;
            grappleAnchor.transform.parent = swingHit.collider.gameObject.transform;
            grappleAnchor.SetActive(true);
            lineRenderer.enabled = true;
           // ChainHixbox.SetActive(true);
            StartCoroutine(SwingingCooldown());
            playerState.SetState(CharacterState.special);
            playerState.SetLeftRightEnabled(false);
            LineGraphicsUpdate();
            //UpdateChain (true);
        }
    }
    public void BreakLine(bool forceAnimation = false, bool usePosition = false, Vector3 cutPosition = default(Vector3))
    {
        if (playerState.GetState() == CharacterState.special)
        {
            grappleAnchor.SetActive(false);
           // ChainHixbox.SetActive(false);
            lineRenderer.enabled = false;
            playerState.SetState(CharacterState.air);
            playerState.SetLeftRightEnabled( true);
            grappleAnchor.transform.parent = null;
           
            //AnimateLineBreak(forceAnimation, usePosition, cutPosition);
        }
    }
    void LineGraphicsUpdate()
    {
       // ChainHixbox.transform.localScale = new Vector3(Vector2.Distance(transform.position, swingPoint), 1, 1);
       // ChainHixbox.transform.position = Vector3.MoveTowards(transform.position, swingPoint, Vector2.Distance(transform.position, swingPoint) / 2);
        Vector2 diff = transform.parent.position - new Vector3(swingPoint.x, swingPoint.y, 0);
        // ChainHixbox.transform.rotation = Quaternion.Euler(0, 0, 90 - Mathf.Atan2(diff.x, diff.y) * Mathf.Rad2Deg);
        lineRenderer.SetPosition(0, transform.parent.position);
        lineRenderer.SetPosition(1, swingPoint);
        lineRenderer.material.mainTextureScale = new Vector2(Vector2.Distance(transform.parent.position, swingPoint), 1);
    }
    void CheckLineBreaks()
    {
        Vector2 dir = new Vector3(swingPoint.x, swingPoint.y, 1) - transform.position;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, Mathf.Infinity, groundMask);
        if (hit.point != swingPoint)
        {
            //AnimateLineBreak(false, false, Vector3.zero, hit.point);
            grappleAnchor.transform.parent = hit.collider.gameObject.transform;
            swingPoint = hit.point;
            swingRadius = hit.distance;
            grappleAnchor.transform.position = swingPoint;
        }

    }
    IEnumerator SwingingCooldown()
    {
        swingEnabled = false;
        yield return new WaitForSeconds(.4f);
        swingEnabled = true;
    }
    public void CancelMovement()
    {
        BreakLine();
    }
}
