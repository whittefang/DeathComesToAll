﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {
    int currentHealth;
    DeathHandler deathHandler;
	// Use this for initialization
	void Start () {
        deathHandler = GetComponent<DeathHandler>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void DealDamage(int damage) {
        currentHealth -= damage;
        if (currentHealth <= 0) {
            deathHandler.Kill();
        }

    }
}
