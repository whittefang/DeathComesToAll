﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrownAxe : MonoBehaviour {
    Rigidbody2D rb;
    SpriteAnimator spriteAnimator;

	// Use this for initialization
	void Awake () {
        
        transform.parent = null;
        rb = GetComponentInChildren<Rigidbody2D>();
        spriteAnimator = GetComponent<SpriteAnimator>();
	}
    void OnEnable() {
        rb.isKinematic = false;
        spriteAnimator.PlayAnimation(0);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Environment") {
            StickIntoWall();
        }
    }

    void StickIntoWall() {
        // stop movement
        spriteAnimator.PlayAnimation(1);
        rb.velocity = Vector2.zero;
        // set to kinematic
        rb.isKinematic = true;

        // turn on stuck in wall axe
        // turn self off 
    }
}
