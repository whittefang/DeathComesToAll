﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState : MonoBehaviour {
    [SerializeField] CharacterState state = CharacterState.ground;

    public SpriteRenderer bodyAnimator;
    public SpriteRenderer handAnimator;

    Rigidbody2D rb;

    bool onWallLeft = false;
    bool doubleJumpRemaining = true;
    bool leftRightMovementEnabled = true;
    bool canAttatchToWall = true;
    bool facingRight = true;
    bool attacking = false;

    int groundedBuffer = 0;
    int groundMask;
    MovementPlayer movementPlayer;
    // Use this for initialization
    void Start () {
        groundMask = 1 << 8;
        rb = GetComponent<Rigidbody2D>();
        movementPlayer = GetComponent<MovementPlayer>();
    }
	
	// Update is called once per frame
	void Update () {
        CheckForGround();
        CheckForWalls();
    }
    //---------------------------------//
    //                         getters //
    //---------------------------------//
    public CharacterState GetState() {
        return state;
    }
   
    public bool IsDoubleJumpRemaining() {
        return doubleJumpRemaining;
    }
    public bool CanAttatchToWall() {
        return canAttatchToWall;
    }
    public void UseUpDoubleJump() {
        doubleJumpRemaining = false;
    }
    public bool IsLeftRightEnabled()
    {
        return leftRightMovementEnabled;
    }
    public bool IsOnWallLeft()
    {
        return onWallLeft;
    }
    public bool IsFacingRight() {
        return facingRight;
    }
    public bool IsAttacking() {
        return attacking;
    }

    //---------------------------------//
    //                         Setters //
    //---------------------------------//
    public void SetAttacking(bool attacking) {
        this.attacking = attacking;
    }
    public void SetLeftRightEnabled(bool enabled) {
        leftRightMovementEnabled = enabled;
    }
    public void SetState(CharacterState newState)
    {
        state = newState;
    }
    public void setFacingRight(bool facingRight)
    {
        if (!attacking)
        {
            this.facingRight = facingRight;
            if (facingRight)
            {
                bodyAnimator.flipX = false;
                handAnimator.flipX = false;
            }
            else
            {
                bodyAnimator.flipX = true;
                handAnimator.flipX = true;
            }
        }
    }
    public void UseGroundJump()
    {
        state = CharacterState.air;
        StartCoroutine(ResetWallBuffer(15));
    }
    public void UseWallJump()
    {
        onWallLeft = false;
        leftRightMovementEnabled = false;
        canAttatchToWall = false;
        doubleJumpRemaining = true;
        state = CharacterState.air;
        StartCoroutine(ResetWallBuffer(5));
        StartCoroutine(ResetLeftRightMovementBuffer(1));
    }


    //---------------------------------//
    // Ground Checking                 //
    //---------------------------------//
    void CheckForGround()
    {
        RaycastHit2D groundCheck = Physics2D.Raycast(transform.position, Vector2.down, 1.2f, groundMask);


        if (groundCheck.collider != null && groundedBuffer <= 0)
        {
            movementPlayer.CancelMovement();
            state = CharacterState.ground;
            doubleJumpRemaining = true;
        }
        else if (groundCheck.collider == null && state != CharacterState.wall && state != CharacterState.special)
        {
            state = CharacterState.air;
        }
        else if (groundedBuffer > 0)
        {
            groundedBuffer--;
        }
    }
    //---------------------------------//
    //        wall Checking            //
    //---------------------------------//
    void CheckForWalls() {

        if (state != CharacterState.ground)
        {

            RaycastHit2D wallCheckRight = Physics2D.Raycast(transform.position + new Vector3(0, 0f, 0), Vector2.right, .6f, groundMask);
            RaycastHit2D wallCheckLeft = Physics2D.Raycast(transform.position + new Vector3(0, 0f, 0), Vector2.left, .6f, groundMask);

            if (wallCheckLeft.collider != null)
            {
                onWallLeft = true;
                AttatchToWall();
            }
            else if (wallCheckRight.collider != null)
            {
                onWallLeft = false;
                AttatchToWall();
            } else if (state != CharacterState.ground && state != CharacterState.special) {
                state = CharacterState.air;
            }
        }

    }
    void AttatchToWall()
    {
        if (state != CharacterState.wall && canAttatchToWall)
        {
            movementPlayer.CancelMovement();
            state = CharacterState.wall;
            rb.velocity = Vector2.zero;
        }
    }

    void OnDisable() {
    }
    //---------------------------------//
    // buffers                         //
    //---------------------------------//
    IEnumerator ResetLeftRightMovementBuffer(int delay) {
        for (int i = 0; i < delay; i++)
        {
            yield return null;
        }
        leftRightMovementEnabled = true;
    }
    
    IEnumerator ResetWallBuffer(int delay)
    {
        canAttatchToWall = false;
        for (int i = 0; i < delay; i++)
        {
            yield return null;
        }
        canAttatchToWall = true;
    }
}

public enum CharacterState {
    ground,
    wall,
    air,
    special
}
