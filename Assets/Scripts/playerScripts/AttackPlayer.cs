﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPlayer : MonoBehaviour {
    InputScript input;
    Weapon weapon;
	// Use this for initialization
	void Start () {
        input = GetComponent<InputScript>();
        input.assignXButton(StartPrimaryAttackPress, StartPrimaryAttackRelease);
        input.assignYButton(StartSecondaryttackPress, StartSecondaryAttackRelease);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void StartPrimaryAttackPress() {
        weapon.PressAttackPrimary();
    }

    void StartPrimaryAttackRelease()
    {
        weapon.ReleaseAttackPrimary();
    }

    void StartSecondaryttackPress()
    {
        weapon.PressAttackSecondary();
    }

    void StartSecondaryAttackRelease()
    {
        weapon.ReleaseAttackSecondary();
    }
    void CancelAttacks() {
        weapon.CancelAttacks();
    }
    public void SetWeapon(Weapon newWeapon) {
        weapon = newWeapon;
    }

    void OnDisable() {
        CancelAttacks();
    }
}
