﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox : MonoBehaviour {

    public InputScript input;

	// Use this for initialization
	void Awake () {
        input = GetComponentInParent<InputScript>();
        gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public bool ComparePlayerID(int otherID) {
        if (input.playerNumber == otherID) {
            return true;
        }
        return false;
    }
    public int getPlayerNumber() {
        return input.playerNumber;
    }
}
