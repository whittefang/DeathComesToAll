﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformFollower : MonoBehaviour {
    public Transform transformToFollow;
    public Vector3 positionOffset;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (transformToFollow != null)
        {
            transform.position = transformToFollow.position + positionOffset;
        }
    }
    public void SetTransformToFollow(Transform newTransformToFollow) {
        transformToFollow = newTransformToFollow;
    }
}
