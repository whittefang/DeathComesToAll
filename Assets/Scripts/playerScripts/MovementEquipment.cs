﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface MovementEquipment  {

    void PressMovement();
    void ReleaseMovement();
    void CancelMovement();

}
