﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpriteAnimation {
    public Sprite[] frames;
    public int timeBetweenFrames =1;
    public AnimationType animationType;	
}
