﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandAnimationController : MonoBehaviour {
    SpriteAnimator handAnimator;
    int neutralAnimKey = 0;
    int runningAnimKey = 1;
    int jumpAnimKey = 2;


    // Use this for initialization
    void Start () {
        //handAnimator.PlayAnimation(0);	
	}
	
    public void PlayAnimation(int key) {
        if (CheckIfCanPlayAnim()) {
            handAnimator.PlayAnimation(key);
        }
    }
    public void PlayNeutral() {
        if (CheckIfCanPlayAnim())
        {
            handAnimator.PlayAnimation(neutralAnimKey);
        }
    }
    public void PlayRunning()
    {
        if (CheckIfCanPlayAnim())
        {
            handAnimator.PlayAnimation(runningAnimKey);
        }
    }

    bool CheckIfCanPlayAnim() {
        bool noAnim = handAnimator.GetCurrentAnimation() == -1;
        bool idle = handAnimator.GetCurrentAnimation() == neutralAnimKey;
        bool run = handAnimator.GetCurrentAnimation() == runningAnimKey;
        bool jump = handAnimator.GetCurrentAnimation() == jumpAnimKey;

        return (noAnim || idle || run || jump);
    }
    public void PlayJump()
    {
        if (CheckIfCanPlayAnim())
        {
            handAnimator.PlayAnimation(jumpAnimKey);
        }
    }
    public void ChangeGenericAnims(int neutralAnimKey, int runningAnimKey, int jumpAnimKey ) {
        // translate current animation into new animation then set them up
        // may just need to set current animation to -1
        int cur = handAnimator.GetCurrentAnimation();
        if (cur == this.neutralAnimKey)
        {
            handAnimator.PlayAnimation(neutralAnimKey);
        }
        else if (cur == this.runningAnimKey)
        {
            handAnimator.PlayAnimation(runningAnimKey);
        }
        else if(cur == this.jumpAnimKey)
        {
            handAnimator.PlayAnimation(jumpAnimKey);
        }

        this.neutralAnimKey = neutralAnimKey;
        this.runningAnimKey = runningAnimKey;
        this.jumpAnimKey = jumpAnimKey;

        
    }
    public void SetAnimator(SpriteAnimator newAnimator) {
        handAnimator = newAnimator;
    }
}
