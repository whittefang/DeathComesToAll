﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour {
    int[] scores;
    int scoreToWin = 10;
    public TransformFollower transformFollower;
    MasterGameObject masterGameObject;
	// Use this for initialization
	void Start () {
        scores = new int[4];
        transformFollower = GetComponent<TransformFollower>();
        masterGameObject = GameObject.Find("MasterGameObject").GetComponent<MasterGameObject>();

	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void addScore(int playerNumber) {
        scores[playerNumber]++;
        CheckForNewScoreLeader(playerNumber);
        CheckForWin(playerNumber);
    }
    void CheckForNewScoreLeader(int playerNumber) {
        int highest = 0;
        int leader = 0;
        for (int i = 0; i < scores.Length; i++)
        {
            if (scores[i] > highest) {
                leader = i;
                highest = scores[i];
            }
        }
        if (scores[playerNumber] >= highest)
        {
            transformFollower.SetTransformToFollow(masterGameObject.GetPlayer(playerNumber).transform.GetChild(0));
        }
    }
    void CheckForWin(int playerNumber) {
        if (scores[playerNumber] >= scoreToWin) {
            // player won the game
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
    

}
