﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostMovement : MonoBehaviour
{
    public GameObject player;
    InputScript input;
    Rigidbody2D rb;
    const float MOVE_SPEED = 15;
	// Use this for initialization
	void Start () {
        input = GetComponent<InputScript>();
        rb = GetComponent<Rigidbody2D>();
        input.SetPlayerNumber(player.GetComponentInChildren<InputScript>().playerNumber);
        input.SetThumbstick(Move);
	}
    void OnEnable() {
        transform.position = player.transform.position;
        StartCoroutine(Respawn());
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Move(float x, float y) {
        if (Mathf.Abs(x) < .2f) {
            x = 0;
        }
        if (Mathf.Abs(y) < .2f)
        {
            y = 0;
        }
        rb.velocity = new Vector2(x * MOVE_SPEED,y * MOVE_SPEED);
    }
    IEnumerator Respawn() {
        yield return new WaitForSeconds(2);

        player.transform.position = transform.position;
        player.SetActive(true);
        gameObject.SetActive(false);
    }
}
