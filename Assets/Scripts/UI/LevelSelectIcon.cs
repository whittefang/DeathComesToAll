﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectIcon : MonoBehaviour {
    Vector3 iconLocation;
    public Sprite previewImage;


	// Use this for initialization
	void Start () {
        iconLocation = transform.position + new Vector3(0, -1, 0);	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public Vector3 GetIconLocation() {
        return iconLocation;
    }
}