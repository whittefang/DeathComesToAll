﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MasterGameObject :MonoBehaviour {
    public bool debugMode = false;
    public GameObject playerPrefab;
    public GameObject axePrefab;
    public GameObject chainSwingPrefab;
    PlayerEquipment[] playerEquipment;
    public  GameObject[] players;
    // Use this for initialization
    void Awake()
    {
        players = new GameObject[4];
        playerEquipment = new PlayerEquipment[4];
        for (int i = 0; i < playerEquipment.Length; i++)
        {
            playerEquipment[i] = new PlayerEquipment(i, WeaponType.axe, MovementType.swingHook);
        }
        // BIG TEMPORARY BLOCK
        if (!debugMode)
        {
            SceneManager.activeSceneChanged += OnLevelChanged;
        }
        else {
            // TEMPORARY
            playerEquipment[0].inGame = true;
            playerEquipment[1].inGame = true;
            
            LoadInPlayers();
        }
    }

    void OnLevelChanged(Scene x, Scene y) {
        if (SceneManager.GetActiveScene().buildIndex >= 0)
        {
            // TEMPORARY
            playerEquipment[0].inGame = true; 
            playerEquipment[1].inGame = true;


            LoadInPlayers();
            
        }
    }

    void LoadInPlayers()
    {
        foreach (PlayerEquipment p in playerEquipment)
        {
            if (p.inGame)
            {
                LoadInPlayer(p);
            }
        }
    }
    void LoadInPlayer(PlayerEquipment player) {
        GameObject newPlayer = Instantiate(playerPrefab, Vector3.zero, Quaternion.identity) as GameObject;
        newPlayer.GetComponentInChildren<InputScript>().SetPlayerNumber(player.playerNumber);
        players[player.playerNumber] = newPlayer;
        SetWeapon(player.selectedWeapon, newPlayer);
        SetMovementEquipment(player.selectedMovement, newPlayer);
    }
    void SetWeapon(WeaponType weaponType, GameObject player) {
        
        GameObject weapon;
        switch (weaponType)
        {
            case WeaponType.axe:
                weapon = Instantiate(axePrefab, player.transform.GetChild(0)) as GameObject;
                player.GetComponentInChildren<AttackPlayer>().SetWeapon(weapon.GetComponentInChildren<AxeWeapon>());
                break;
            case WeaponType.spear:
                break;
            case WeaponType.bow:
                break;
            case WeaponType.fist:
                break;
            default:
                weapon = Instantiate(axePrefab, player.transform.GetChild(0)) as GameObject;
                player.GetComponentInChildren<AttackPlayer>().SetWeapon(weapon.GetComponentInChildren<AxeWeapon>());
                break;

        }
    }
    void SetMovementEquipment(MovementType weaponType, GameObject player)
    {

        GameObject movementEquipment;
        switch (weaponType)
        {
            case MovementType.swingHook:
                movementEquipment = Instantiate(chainSwingPrefab, player.transform.GetChild(0)) as GameObject;
                player.GetComponentInChildren<MovementPlayer>().SetMovement(movementEquipment.GetComponentInChildren<ChainSwingMovement>());
                break;
            case MovementType.grappleHook:
                break;
            case MovementType.glider:
                break;
            case MovementType.dasher:
                break;
            default:
                movementEquipment = Instantiate(chainSwingPrefab, player.transform.GetChild(0)) as GameObject;
                player.GetComponentInChildren<MovementPlayer>().SetMovement(movementEquipment.GetComponentInChildren<ChainSwingMovement>());
                break;

        }

    }
    public void SetPlayerEquipment(int playerNumber, int weapon, int movement, int color) {
        playerEquipment[playerNumber].selectedWeapon = (WeaponType)weapon;
        playerEquipment[playerNumber].selectedMovement = (MovementType)movement;
        // TODO: set color
    }
    public void AddPlayerToGame(int playerNumber) {
        playerEquipment[playerNumber].inGame = true;
    }
    public void RemovePlayerFromGame(int playerNumber)
    {
        playerEquipment[playerNumber].inGame = false;

    }
    public GameObject GetPlayer(int playerNumber) {
        return players[playerNumber];
    }
}

 
class PlayerEquipment
{
    public bool inGame;
    public int playerNumber;
    public WeaponType selectedWeapon;
    public MovementType selectedMovement;

 

    public PlayerEquipment() {
        inGame = false;
    }
    public PlayerEquipment(int playerNumber, WeaponType selectedWeapon, MovementType selectedMovement) {
        inGame = false;
        this.playerNumber = playerNumber;
        this.selectedMovement = selectedMovement;
        this.selectedWeapon = selectedWeapon;
    }

}

enum MovementType{
    swingHook,
    grappleHook,
    glider,
    dasher
}
enum WeaponType {
    axe,
    bow,
    spear,
    fist
}
